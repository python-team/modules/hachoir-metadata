hachoir-metadata (1.3.3-3) UNRELEASED; urgency=medium

  [ Ondřej Nový ]
  * Fixed homepage (https)
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove trailing whitespaces
  * d/control: Remove ancient X-Python-Version field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Mon, 04 Jan 2021 16:59:47 -0500

hachoir-metadata (1.3.3-2) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Michel Casabona ]
  * bump Standards-Version to 3.9.5
  * debian/{control,rules}
    - Switch to dh_python2
  * debian/{compat,control}
    - Bump debhelper compatibility level to 9
  * debian/control: fix typo in package description (Closes: #734181)

 -- Michel Casabona <michel.casabona@free.fr>  Mon, 15 Sep 2014 01:03:04 +0200

hachoir-metadata (1.3.3-1) unstable; urgency=low

  * New upstream release
  * remove outdated URL from hachoir-metadata man page
  * bump Standards-Version to 3.9.1
    - reference GPL-2 in the copyright file
  * Switch to dpkg-source 3.0 (quilt) format
  * debian/rules: clean hachoir_metadata/qt/dialog_ui.py from upstream

 -- Michel Casabona <michel.casabona@free.fr>  Sat, 20 Nov 2010 13:10:55 +0100

hachoir-metadata (1.3.2-1) unstable; urgency=low

  [ Michel Casabona ]
  * New upstream release
    - hachoir-metadata-gtk: fix typos, accept filename as argument
      (closes: #505426, #505427).
  * bump Standards-Version to 3.8.4
  * bump python-hachoir-parser minimum required version to 1.3
  * debian/control:
    - Build-Depends on pyqt4-dev-tools
    - Suggests python-gtk2 for hachoir-metadata-gtk
    - Suggests python-qt4 for hachoir-metadata-qt
    - Depends on ${misc:Depends}
    - Build-Depends-Indep on python-all instead of python-all-dev
    - new Homepage

  [ Sandro Tosi ]
  * debian/control
    - switch Vcs-Browser field to viewsvn

 -- Michel Casabona <michel.casabona@free.fr>  Tue, 02 Mar 2010 21:24:41 +0100

hachoir-metadata (1.2-1) unstable; urgency=low

  * New upstream release
  * bump python-hachoir-parser's minimum required version to 1.2
  * bump Standards-Version to 3.8.0
  * update manpages

 -- Michel Casabona <michel.casabona@free.fr>  Sun, 28 Sep 2008 17:28:05 +0200

hachoir-metadata (1.1-1) unstable; urgency=low

  [ Michel Casabona ]
  * New upstream release
  * upgrade Standards-Version to 3.7.3
  * debian/copyright: change (C) line format for lintian
  * add minimal manpage for new program hachoir-metadata-gtk

  [ Piotr Ożarowski ]
  * Homepage field added
  * Rename XS-Vcs-* fields to Vcs-* (dpkg supports them now)

 -- Michel Casabona <michel.casabona@free.fr>  Thu, 24 Apr 2008 01:29:10 +0200

hachoir-metadata (1.0.1-1) unstable; urgency=low

  * New upstream release
  * Suggest python-profiler

 -- Michel Casabona <michel.casabona@free.fr>  Fri, 13 Jul 2007 03:48:25 +0200

hachoir-metadata (0.10.0-1) unstable; urgency=low

  [ Piotr Ożarowski ]
  * add debian/watch file

  [ Michel Casabona ]
  * New upstream release

 -- Michel Casabona <michel.casabona@free.fr>  Sun, 06 May 2007 23:41:23 +0200

hachoir-metadata (0.8.2-1) unstable; urgency=low

  * New upstream release
  * python-setuptools is now optional, remove it from Build-Depends
  * no longer requires hachoir-parser to build

 -- Michel Casabona <michel.casabona@free.fr>  Sun, 28 Jan 2007 15:19:09 +0100

hachoir-metadata (0.7.0-2) UNRELEASED; urgency=low

  * convert to python-support

 -- Michel Casabona <michel.casabona@free.fr>  Mon, 22 Jan 2007 17:57:05 +0100

hachoir-metadata (0.7.0-1) UNRELEASED; urgency=low

  * source package automatically created by stdeb

 -- Michel Casabona <michel.casabona@free.fr>  Mon, 22 Jan 2007 17:56:41 +0100
